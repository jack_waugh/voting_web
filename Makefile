# Remember to preserve tabs in Makefile.

TGTDIR = ../personal_website/public/voting

$(TGTDIR): Makefile
	rm -rf $(TGTDIR)
	ln -s ../../voting/public $@
	cmp $@/index.html public/index.html # check correctness of link.
